import sage.all
import sage.graphs.digraph

import youngd

# Given k and n, return the initial quiver associated to rectangular
# plabic cluster, following Rietsch-Williams.
	
def rectangular_quiver(k, n):
	
	# Store the indices (i,j) of a size k(n-k) square grid as keys in a
	# dictionary pos, where values are their positions in the total order
	# that arranges the columns to be decreasing chains when read from top
	# to bottom, and decreasing along rows when read from left to right.
	
	pos = {}
	counter = 0
	for j in reversed(range(1,n-k+1)):
		for i in reversed(range(1,k+1)):
			pos[(i,j)] = counter
			counter = counter + 1
	
	# Construct the size k(n-k)+1 exchange matrix B of the rectangular quiver
	
	B = []
	for j in reversed(range(1,n-k+1)):
		for i in reversed(range(1,k+1)):
			row = [0]*(k*(n-k)+1)
			if 1 < i < k and 1 < j < n-k :
				row[pos[(i-1,j-1)]] = 1
				row[pos[(i,j+1)]] = 1
				row[pos[(i+1,j)]] = 1
				row[pos[(i,j-1)]] = -1
				row[pos[(i-1,j)]] = -1
				row[pos[(i+1,j+1)]] = -1
			elif 1 < i < k and j == n-k :
				row[pos[(i-1,j-1)]] = 1
				row[pos[(i,j-1)]] = -1
			elif i == k and 1 < j < n-k :
				row[pos[(i-1,j-1)]] = 1
				row[pos[(i-1,j)]] = -1
			elif 1 < i < k and j == 1 :
				row[pos[(i,j+1)]] = 1
				row[pos[(i+1,j)]] = 1
				row[pos[(i-1,j)]] = -1
				row[pos[(i+1,j+1)]] = -1
			elif i == 1 and 1 < j < n-k :
				row[pos[(i,j+1)]] = 1
				row[pos[(i+1,j)]] = 1
				row[pos[(i,j-1)]] = -1
				row[pos[(i+1,j+1)]] = -1
			elif i == 1 and j == n-k :
				row[pos[(i,j-1)]] = -1
			elif i == k and j == n-k :
				row[pos[(i-1,j-1)]] = 1
			elif i == k and j == 1:
				row[pos[(i-1,j)]] = -1
			elif i == 1 and j == 1:
				row[pos[(i+1,j)]] = 1
				row[pos[(i,j+1)]] = 1
				row[pos[(i+1,j+1)]] = -1
				row[k*(n-k)] = -1
			B.append(row)
	row = [0]*(k*(n-k)+1)
	row[pos[(1,1)]] = 1
	B.append(row)

	# Construct a directed graph from B
	
	for i in range(len(B)):
		for j in range(len(B[i])):
			if B[i][j] < 0:
				B[i][j] = 0
	Q = sage.graphs.digraph.DiGraph(sage.all.matrix(B), multiedges=True)
			
	return Q
	
# Given k and n, return the list d_label of labels of vertices
# corresponding to rectangular diagrams in the rectangular quiver of
# type (k,n).

def rectangular_labels(k, n):
	
	d_label = []
	
	for i in range(n-k):
		for j in range(k):
			d_label.append(tuple(youngd.vstep_of_rect(k-j, n-k-i, k, n)))
		
	d_label.append(tuple(youngd.vstep_of_rect(0, 0, k, n)))	
	
	return d_label

# Given a quiver Q with list of frozen nodes f, mutate Q
# along the node v. We assume v has two incoming and two outgoing
# edges and is not frozen, so that it corresponds to a 3-term
# Plucker relation according to Rietsch-Williams.
	
def mutate(Q, f, v):

	v_in = Q.neighbors_in(v)
	v_out = Q.neighbors_out(v)
	
	# Add edges of type a -> v -> b with a in v_in, b in v_out
	# and at least one among v_in and v_out not frozen, and
	# make sure to erase newly created oriented 2-loops
	
	for a in v_in:
		for b in v_out:
			if (a in f) and (b in f):
				continue
			else:
				if Q.has_edge(b, a):
					Q.delete_edge(b, a)
				else:
					Q.add_edge(a, b)
	
	# Flip orientation of edges touching v
	
	Q.add_edge(v, v_in[0])
	Q.delete_edge(v_in[0], v)
	Q.add_edge(v, v_in[1])
	Q.delete_edge(v_in[1], v)
	
	Q.add_edge(v_out[0], v)
	Q.delete_edge(v, v_out[0])
	Q.add_edge(v_out[1], v)
	Q.delete_edge(v, v_out[1])
	
	return Q
