import sage.all
from sage.symbolic.expression_conversions import laurent_polynomial

import youngd


# Make sure that SageMath understands a rational function f with monomial
# denominator as a Laurent polynomial in the ring R_laurent, and not just as
# element of the function field
	
def force_laurent(f, R_laurent):

	num = f.numerator()
	den = f.denominator()
	
	den_inverse_laurent = 1
	for t in den.variables():
		t = sage.all.var(t)
		den_inverse_laurent = den_inverse_laurent * laurent_polynomial(1/t, ring=R_laurent)
	
	return num * den_inverse_laurent


# Given k, n and the Plucker variables p, return the initial Laurent polynomial
# associated to the rectangular plabic cluster of Gr(k,n), according
# to Marsh-Rietsch

def rectangular_potential(k, n, p):
				
	term1 = p[tuple(youngd.vstep_of_rect(1, 1, k, n))] * p[tuple(youngd.vstep_of_rect(0, 0, k, n))]**-1
	
	term2 = 0
	for i in range(2,k+1):
		for j in range(1,n-k+1):
			term2 = term2 + p[tuple(youngd.vstep_of_rect(i, j, k, n))] * p[tuple(youngd.vstep_of_rect(i-2, j-1, k, n))] * p[tuple(youngd.vstep_of_rect(i-1, j-1, k, n))]**-1 * p[tuple(youngd.vstep_of_rect(i-1, j, k, n))] **-1
	
	term3 = p[tuple(youngd.vstep_of_rect(k-1, n-k-1, k, n))] * p[tuple(youngd.vstep_of_rect(k, n-k, k, n))]**-1
	
	term4 = 0
	for i in range(1,k+1):
		for j in range(2,n-k+1):
			term4 = term4 + p[tuple(youngd.vstep_of_rect(i, j, k, n))] * p[tuple(youngd.vstep_of_rect(i-1, j-2, k, n))] * p[tuple(youngd.vstep_of_rect(i-1, j-1, k, n))]**-1 * p[tuple(youngd.vstep_of_rect(i, j-1, k, n))]**-1
	
	W = term1 + term2 + term3 + term4
	
	return W
