import copy
import itertools
import math

import sage.all
import sage.rings

import critpoints
import plucker
import schurpol
import spectrum
import walk
import youngd


# Take in input k and n

print('Input k: ')
k = input()
print('Input n: ')
n = input()

# Ask if user wants pictures of critical values of W and subsets
# of critical values corresponding to different cluster charts

print('Pictures? ')
show_spectrum = input()

# Compute N = {n choose n-k}, total number of critical points of mirror
# Landau-Ginzburg superpotential on Gr(n-k, n)

N = math.factorial(n) / ( math.factorial(k)*math.factorial(n-k) )
print('Total number of critical points: {}'.format(N))

# Construct the homogeneous coordinate ring of the ambient
# projective space for the Plucker embedding, and inject its generators
# in the variables namespace

proj_ring = sage.rings.all.PolynomialRing(sage.all.QQ, 'x', N)
proj_ring.inject_variables(verbose=False)
	
# Define the Plucker coordinates corresponding to the generators
# of the homogeneous coordinate ring of the ambient projective space
	
p = plucker.variables(n-k, n, proj_ring)
	
# Construct the Plucker relations describing the homogeneous coordinate ring
# of Gr(n-k,n) in the Plucker embedding
	
relations = plucker.rel_list(n-k, n, p)

# Run a random walk that constructs the list of plabic cluster charts
# of the Grassmannian Gr(n-k,n)

cluster_list = walk.random_walk(n-k, n, p, relations)

# Initialize roots of unity

roots = critpoints.basic_roots(n-k, n, critpoints.cyclotomic(n-k, n))

# Construct the list e of eigenvalues of multiplication by c_1 in
# the quantum cohomology QH(Gr(k,n)), or equivalently of critical
# values of Marsh-Rietsch Landau-Ginzburg superpotential W

e = []
for S in itertools.combinations(range(1,n+1), n-k):
	sum_S = 0
	for t in S:
		sum_S = sum_S + roots[t-1]
	e.append(complex(n*sum_S))

lagrangian_weights = []
i = 0
for C in cluster_list:

	# Initialize the list c_values of critical values of critical points of W
	# contained in the cluster chart corresponding to the cluster C
	
	c_values = []
	
	crit_counter = 0
	for S in itertools.combinations(range(1,n+1), n-k):
		roots_S = []
		sum_S = 0
		for t in S:
			roots_S.append(roots[t-1])
			sum_S = sum_S + roots[t-1]
		is_in_chart = True
		for d in C:
			d = youngd.vstep_to_part(n-k, n, d)
			if (schurpol.schur_sage(n-k, n, [d])[tuple(d)])(roots_S) == 0:
				is_in_chart = False
				break
		if is_in_chart == True:
			crit_counter = crit_counter + 1
			c_values.append(complex(n*sum_S))
	print('Cluster chart #{} = {}'.format(i, cluster_list[i]))
	print('The chart contains {} critical points'.format(crit_counter))
	if crit_counter not in lagrangian_weights:
		lagrangian_weights.append(crit_counter)
	if show_spectrum == 1:
		spectrum.spectrum_combined_picture(c_values, e, "BranesGr({},{})Cluster{}".format(k,n,i), k, n)
	i = i + 1
	
print('There are {} Lagrangian weights: {}'.format(len(lagrangian_weights), lagrangian_weights))
