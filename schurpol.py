import sage.combinat.sf.sf

# Given a list of Young diagrams in k x (n-k) grid, return a dictionary where
# they are keys with value the corresponding Schur polynomial in k variables,
# as SageMath symmetric functions.

def schur_sage(k, n, diagrams):

	d = {}
	
	# initialize k variables of Schur polynomials
	
	z = []
	for i in range(1,k+1):
		z.append('z{}'.format(i))

	s = sage.combinat.sf.sf.SymmetricFunctions(sage.all.QQ).schur()
	
	for y in diagrams:
		d[tuple(y)] = s(y).expand(k, alphabet=z)
		
	return d
