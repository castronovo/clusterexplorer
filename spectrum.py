import matplotlib.pyplot as plt
import sympy.functions.elementary.complexes as cpx

# Given two lists branes and e of complex numbers and a string name,
# save image of the corresponding points on complex plane in
# "[name]SpectrumGr(k,n).png", where e are red dots and branes are blue crosses

def spectrum_combined_picture(branes, e, name, k, n):

	e_real_parts = []
	e_imaginary_parts = []
	branes_real_parts = []
	branes_imaginary_parts = []

	for z in e:
		z = complex(z)
		e_real_parts.append(cpx.re(z))
		e_imaginary_parts.append(cpx.im(z))
	for z in branes:
		z = complex(z)
		branes_real_parts.append(cpx.re(z))
		branes_imaginary_parts.append(cpx.im(z))
			
	plt.clf()
	plt.xlim(-3*n, 3*n)
	plt.ylim(-3*n, 3*n)
	plt.gca().set_aspect('equal', adjustable='box')
	plt.plot(e_real_parts, e_imaginary_parts, 'ro', markersize=2)
	plt.plot(branes_real_parts, branes_imaginary_parts, 'bx', markersize=5)

	plt.savefig('{}.png'.format(name))
